﻿namespace Game.Core
{
    using System.Linq;

    using Game.Logic;

    using UnityEditor;

    using UnityEngine;

    [CustomEditor(typeof(TargetBehaviour))]
    public class TargetEditor : Editor
    {
        #region Public Methods and Operators

        public void OnSceneGUI()
        {
            var targetBehaviour = this.target as TargetBehaviour;
            if (targetBehaviour == null)
            {
                return;
            }

            var connectionSettings = FindObjectOfType<ConnectionSettings>();
            if (connectionSettings == null)
            {
                return;
            }

            var targetPosition = targetBehaviour.transform.position;

            foreach (var zone in connectionSettings.Zones)
            {
                var outerDistance = connectionSettings.GetDistanceFromSignal(zone.MinimumSignal);
                Handles.DrawWireDisc(targetPosition, Vector3.forward, outerDistance);
                //var newOuterDistance = Handles.ScaleValueHandle(
                //    outerDistance,
                //    targetPosition + new Vector3(outerDistance, 0, 0),
                //    Quaternion.Euler(90, 180, 90),
                //    HandleUtility.GetHandleSize(targetPosition),
                //    Handles.CylinderCap,
                //    HandleUtility.GetHandleSize(targetPosition));

                //zone.MinimumSignal = connectionSettings.GetSignalFromDistance(newOuterDistance);
            }
        }

        #endregion
    }
}