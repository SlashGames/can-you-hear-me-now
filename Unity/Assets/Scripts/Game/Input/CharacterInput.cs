﻿namespace Game.Features.Input
{
    using Game.Logic;
    using Game.Utils;

    using UnityEngine;

#if UNITY_STANDALONE
	using XInputDotNetPure;
#endif

    public class CharacterInput : MonoBehaviour
    {
        #region Fields

        public int InputIndex;

        #endregion

        #region Public Properties

        protected void Update()
        {
            this.HasInput = this.Direction.magnitude > 0.1f;
        }

        public Vector2 Direction
        {
            get
            {
                var keyboardX = Input.GetAxis("Horizontal_" + this.InputIndex);
                var keyboardY = Input.GetAxis("Vertical_" + this.InputIndex);

#if UNITY_STANDALONE
				var controllerX = (this.InputIndex % 2 == 0)
									  ? this.GamePadState.ThumbSticks.Left.X
									  : this.GamePadState.ThumbSticks.Right.X;
				var controllerY = (this.InputIndex % 2 == 0)
									  ? this.GamePadState.ThumbSticks.Left.Y
									  : this.GamePadState.ThumbSticks.Right.Y;
#else
                var controllerX = 0;
                var controllerY = 0;
#endif

                var x = Mathf.Abs(keyboardX) > 0.01f ? keyboardX : controllerX;
                var y = Mathf.Abs(keyboardY) > 0.01f ? keyboardY : controllerY;

                return new Vector2(x, y).normalized;
            }
        }

        public BooleanUnityEvent HasInputChanged;

        private bool hasInput;

        public bool HasInput
        {
            get
            {
                return this.hasInput;
            }
            set
            {
                if (value == this.hasInput)
                {
                    return;
                }

                this.hasInput = value;

                this.HasInputChanged.Invoke(this.hasInput);
            }
        }

        #endregion

        #region Properties

#if UNITY_STANDALONE
		private GamePadState GamePadState
		{
			get
			{
				var playerIndex = (PlayerIndex)(this.InputIndex / 2);
				return GamePad.GetState(playerIndex);
			}
		}
#endif

        #endregion
    }
}