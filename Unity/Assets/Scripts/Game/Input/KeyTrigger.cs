﻿namespace Game.Features.Input
{
    using System;

    using UnityEngine;
    using UnityEngine.Events;

    public class KeyTrigger : MonoBehaviour
    {
        #region Fields

        public KeyCode KeyCode;

        public KeyPressedEvent KeyPressed;

        #endregion

        #region Public Methods and Operators

        public void Update()
        {
            if (Input.GetKeyDown(this.KeyCode))
            {
                this.KeyPressed.Invoke();
            }
        }

        #endregion

        [Serializable]
        public class KeyPressedEvent : UnityEvent
        {
        }
    }
}