﻿namespace Game.Core
{
    using System;
    using System.Collections;

    using Game.Logic;
    using Game.UI.IncomingCall;

    using Network;

    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.SceneManagement;

    using Random = UnityEngine.Random;

    public class GameBehaviour : MonoBehaviour
    {
        #region Fields

        public CameraMovement CameraMovement;

        public ConnectionSettings ConnectionSettings;

        public DataTransfer DataTransfer;

        public string GameOverSceneId = "Game Over";

        public HomeBehaviour Home;

        public string IncomingCallSceneId = "Incoming Call";

        /// <summary>
        ///   Initial battery duration (in s).
        /// </summary>
        [Tooltip("Initial battery duration (in s).")]
        public float InitialBatteryDuration = 60.0f;

        public string LevelSceneId = "Level";

        public LostEvent Lost;

        public PlayerBehaviour Player;

        /// <summary>
        ///   Required duration to stay in connected zone to win the game (in s).
        /// </summary>
        [Tooltip("Required duration to stay in connected zone to win the game (in s).")]
        public float RequiredConnectedDuration = 5.0f;

        public SpawnPosition SpawnPosition;

        public TargetBehaviour Target;

        public TargetBehaviour[] Targets;

        public WonEvent Won;

        private IncomingCallBehaviour incomingCallWindow;

        private bool isPlayerSpawned;

        private float remainingBatteryDuration;

        #endregion

        #region Properties

        /// <summary>
        ///   Duration the player is connected (in s).
        /// </summary>
        public float ConnectedDuration { get; private set; }

        public float ConnectionStrength
        {
            get
            {
                if (this.Player != null && this.Target != null && this.ConnectionSettings != null)
                {
                    var distance = (this.Target.transform.position - this.Player.transform.position).magnitude;
                    var connectionStrength = this.ConnectionSettings.GetSignalFromDistance(distance);
                    return connectionStrength;
                }
                return 0;
            }
        }

        /// <summary>
        ///   Remaining battery duration (in s).
        /// </summary>
        public float RemainingBatteryDuration
        {
            get
            {
                return this.remainingBatteryDuration;
            }
            private set
            {
                this.remainingBatteryDuration = value;

                if (this.DataTransfer != null)
                {
                    this.DataTransfer.BatteryRatio = this.RemainingBatteryDuration / this.InitialBatteryDuration;
                }
            }
        }

        #endregion

        #region Public Methods and Operators

        [ContextMenu("Call incoming")]
        public void OnCallIncoming()
        {
            // Open window.
            this.StartCoroutine(this.OpenCallIncomingWindow());
        }

        [ContextMenu("Loose Game")]
        public void OnGameLost()
        {
            Debug.Log("Game lost");
            this.Lost.Invoke();

            if (DataTransfer.Local != null)
            {
                DataTransfer.Local.RpcBatteryEmpty();
            }

            this.OnGameEnded();
        }

        [ContextMenu("Win Game")]
        public void OnGameWon()
        {
            Debug.Log("Game won");
            this.Won.Invoke();

            if (DataTransfer.Local != null)
            {
                DataTransfer.Local.RpcWin();
            }

            this.OnGameEnded();
        }

        #endregion

        #region Methods

        protected void Reset()
        {
            this.FindDataTransfer();
        }

        protected void Start()
        {
            this.FindDataTransfer();

            if (this.ConnectionSettings == null)
            {
                this.ConnectionSettings = FindObjectOfType<ConnectionSettings>();
            }

            if (!string.IsNullOrEmpty(this.LevelSceneId))
            {
                this.StartCoroutine(this.LoadLevel());
            }

            DataTransfer.OnIncommingCall += this.OnCallIncoming;
            DataTransfer.OnCallRefused += this.OnHangUpCall;
            DataTransfer.OnCallAccepted += this.OnAnswerCall;

            // Hide player.
            if (this.Player != null)
            {
                this.Player.gameObject.SetActive(false);
            }

            // Initialize phone.
            this.RemainingBatteryDuration = this.InitialBatteryDuration;
        }

        protected void Update()
        {
            if (this.Player == null || !this.Player.IsCalling)
            {
                return;
            }

            var connectionStrength = this.ConnectionStrength;

            // Update signal.
            if (this.DataTransfer != null)
            {
                this.DataTransfer.UpdateConnectionValue(connectionStrength);
            }

            if (this.ConnectionSettings != null && this.ConnectionSettings.Zones.Length > 0)
            {
                var innerZone = this.ConnectionSettings.Zones[0];
                if (connectionStrength >= innerZone.MinimumSignal)
                {
                    this.ConnectedDuration += Time.deltaTime;

                    // Check for win condition.
                    if (this.ConnectedDuration >= this.RequiredConnectedDuration)
                    {
                        this.OnGameWon();
                    }
                }
                else
                {
                    // Reset connected duration.
                    this.ConnectedDuration = 0;
                }
            }

            // Lower battery.
            this.RemainingBatteryDuration -= Time.deltaTime;
            if (this.RemainingBatteryDuration <= 0)
            {
                this.OnGameLost();
            }
        }

        private void FindDataTransfer()
        {
            if (this.DataTransfer == null)
            {
                this.DataTransfer = DataTransfer.DesktopInstance;
            }
        }

        private void FindSpawnPosition()
        {
            this.SpawnPosition = FindObjectOfType<SpawnPosition>();
        }

        private void FindTargets()
        {
            this.Targets = FindObjectsOfType<TargetBehaviour>();
        }

        private IEnumerator LoadLevel()
        {
            yield return SceneManager.LoadSceneAsync(this.LevelSceneId, LoadSceneMode.Additive);

            // Find level things.
            this.FindTargets();
            this.FindSpawnPosition();

            // Move home to spawn position.
            if (this.SpawnPosition != null && this.Home != null)
            {
                this.Home.transform.position = this.SpawnPosition.transform.position;
            }

            // Focus camera on home.
            if (this.CameraMovement != null && this.Home != null)
            {
                this.CameraMovement.TargetTransform = this.Home.CameraFocus;
            }
        }

        private void OnAnswerCall()
        {
            SceneManager.UnloadScene(this.IncomingCallSceneId);

            if (!this.isPlayerSpawned)
            {
                this.StartCoroutine(this.StartIntro());
            }
            else
            {
                this.StartCalling();
            }
        }

        private void OnGameEnded()
        {
            if (this.Player != null)
            {
                this.Player.IsCalling = false;
            }
        }

        private void OnHangUpCall()
        {
            SceneManager.UnloadScene(this.IncomingCallSceneId);
        }

        private IEnumerator OpenCallIncomingWindow()
        {
            var sceneLoad = SceneManager.LoadSceneAsync(this.IncomingCallSceneId, LoadSceneMode.Additive);
            yield return sceneLoad;

            this.incomingCallWindow = FindObjectOfType<IncomingCallBehaviour>();
            this.incomingCallWindow.Answer.AddListener(this.OnAnswerCall);
            this.incomingCallWindow.HangUp.AddListener(this.OnHangUpCall);
        }

        private void StartCalling()
        {
            // Choose target.
            if (this.Targets.Length != 0)
            {
                this.Target = this.Targets[Random.Range(0, this.Targets.Length)];
            }
            else
            {
                Debug.LogWarning("No targets found in scene.");
            }

            if (this.Player != null)
            {
                this.Player.IsCalling = true;
            }

            this.RemainingBatteryDuration = this.InitialBatteryDuration;
        }

        private IEnumerator StartIntro()
        {
            yield return new WaitForSeconds(1);

            // Enable home.
            if (this.Home != null)
            {
                this.Home.gameObject.SetActive(true);
            }

            yield return new WaitForSeconds(8);

            // Spawn player.
            if (this.Player != null)
            {
                this.Player.gameObject.SetActive(true);
                if (this.SpawnPosition != null)
                {
                    this.Player.transform.position = this.SpawnPosition.transform.position;
                }
                else
                {
                    Debug.LogWarning("No spawn position set.");
                }
            }
            else
            {
                Debug.LogWarning("No player transform set.");
            }
            this.isPlayerSpawned = true;

            // Focus camera on player.
            if (this.CameraMovement != null && this.Player != null)
            {
                this.CameraMovement.TargetTransform = this.Player.CameraFocus;
            }

            this.StartCalling();
        }

        #endregion

        [Serializable]
        public class LostEvent : UnityEvent
        {
        }

        [Serializable]
        public class WonEvent : UnityEvent
        {
        }
    }
}