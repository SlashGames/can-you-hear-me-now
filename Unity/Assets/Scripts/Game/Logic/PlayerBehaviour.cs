﻿namespace Game.Logic
{
    using Game.Utils;

    using UnityEngine;

    public class PlayerBehaviour : MonoBehaviour
    {
        #region Fields

        public Transform CameraFocus;

        public BooleanUnityEvent IsCallingChanged;

        private bool isCalling;

        #endregion

        #region Properties

        public bool IsCalling
        {
            get
            {
                return this.isCalling;
            }
            set
            {
                if (value == this.isCalling)
                {
                    return;
                }

                this.isCalling = value;

                this.IsCallingChanged.Invoke(this.isCalling);
            }
        }

        #endregion
    }
}