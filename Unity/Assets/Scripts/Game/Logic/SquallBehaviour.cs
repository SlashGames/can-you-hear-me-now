﻿namespace Game.Logic
{
	using UnityEngine;

	/// <summary>
	///   Adds a force to all characters.
	/// </summary>
	public class SquallBehaviour : MonoBehaviour
	{
		#region Fields

		public Vector2 Direction = Vector2.down;

		public float Force = 1.0f;

		#endregion

		#region Methods

		private void FixedUpdate()
		{
			var force = this.Direction * this.Force * Time.deltaTime;

			WindManager.Instance.SetDirection(Direction);

			// Get rigidbodies.
			var rigidbodies = FindObjectsOfType<Rigidbody2D>();
			foreach (var rigidbody in rigidbodies)
			{
				rigidbody.AddRelativeForce(force);
			}
		}

		#endregion
	}
}