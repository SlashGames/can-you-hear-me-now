﻿namespace Game.Logic
{
    using System;
    using System.Collections.Generic;

    using Game.Utils;

    using UnityEngine;
    using UnityEngine.Events;

    using Random = UnityEngine.Random;

    public class SquallManager : MonoBehaviour
    {
        #region Fields

        private readonly List<Squall> squalls = new List<Squall>();

        /// <summary>
        ///   Cooldown between squalls (in s).
        /// </summary>
        [MinMaxRange(0, 60)]
        [Tooltip("Cooldown between squalls (in s)")]
        public MinMaxRange CooldownBetweenSqualls;

        /// <summary>
        ///   Initial cooldown till first squall.
        /// </summary>
        public float InitialCooldown = 5.0f;

        public SquallCreatedEvent SquallCreated;

        /// <summary>
        ///   Duration of squalls (in s).
        /// </summary>
        [MinMaxRange(0, 60)]
        public MinMaxRange SquallDuration;

        /// <summary>
        ///   Force of squall (in m * a).
        /// </summary>
        [MinMaxRange(100, 10000)]
        public MinMaxRange SquallForce;

        public GameObject SquallPrefab;

        public Transform SquallRoot;

        private float remainingCooldown;

        #endregion

        #region Methods

        protected void Reset()
        {
            if (this.SquallRoot == null)
            {
                this.SquallRoot = this.transform;
            }
        }

        protected void Start()
        {
            this.remainingCooldown = this.InitialCooldown;
        }

        protected void Update()
        {
            this.remainingCooldown -= Time.deltaTime;
            if (this.remainingCooldown <= 0)
            {
                this.CreateSquall();
                this.remainingCooldown = this.CooldownBetweenSqualls.GetRandomValue();
            }

            // Despawn existing squalls.
            for (int index = this.squalls.Count - 1; index >= 0; --index)
            {
                var squall = this.squalls[index];
                squall.RemainingDuration -= Time.deltaTime;
                if (squall.RemainingDuration <= 0)
                {
                    this.DestroySquall(squall);
                }
            }

            WindManager.Instance.SetWindStrength(this.squalls.Count > 0 ? 1 : 0);
        }

        private void CreateSquall()
        {
            // Determine squall direction and force.
            Vector2 direction = GetDirection();

            var force = this.SquallForce.GetRandomValue();

            var squallGameObject = Instantiate(this.SquallPrefab);
            squallGameObject.transform.SetParent(this.SquallRoot, false);
            var squallBehaviour = squallGameObject.GetComponent<SquallBehaviour>();
            if (squallBehaviour == null)
            {
                squallBehaviour = squallGameObject.AddComponent<SquallBehaviour>();
            }
            squallBehaviour.Direction = direction;
            squallBehaviour.Force = force;

            var squall = new Squall()
            {
                Representation = squallGameObject,
                RemainingDuration = this.SquallDuration.GetRandomValue()
            };
            this.squalls.Add(squall);

            this.SquallCreated.Invoke(new SquallCreatedEventData() { Direction = direction, Force = force });
        }

        private void DestroySquall(Squall squall)
        {
            Destroy(squall.Representation);

            this.squalls.Remove(squall);
        }

        private static Vector2 GetDirection()
        {
            int n = Random.Range(0, 3);
            switch (n)
            {
                case 0:
                    return new Vector2(+1, +1).normalized;
                case 1:
                    return new Vector2(-1, +1).normalized;
                case 2:
                    return new Vector2(+1, -1).normalized;
                case 3:
                    return new Vector2(-1, -1).normalized;
            }

            return new Vector2(1, 1);
        }

        #endregion

        [Serializable]
        public class SquallCreatedEvent : UnityEvent<SquallCreatedEventData>
        {
        }

        public class SquallCreatedEventData
        {
            #region Properties

            public Vector2 Direction { get; set; }

            public float Force { get; set; }

            #endregion
        }

        private class Squall
        {
            #region Fields

            public GameObject Representation;

            #endregion

            #region Properties

            public float RemainingDuration { get; set; }

            #endregion
        }
    }
}