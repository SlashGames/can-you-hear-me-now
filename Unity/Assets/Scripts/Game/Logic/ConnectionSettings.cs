﻿namespace Game.Logic
{
	using System;

	using UnityEngine;

	public class ConnectionSettings : MonoBehaviour
	{
		#region Fields

		/// <summary>
		///   Above this distance the signal is zero.
		/// </summary>
		public float MaxDistance = 100.0f;

		public ConnectionZone[] Zones;

		public static ConnectionSettings Instance;

		#endregion

		#region Public Methods and Operators

		private void Awake()
		{
			Instance = this;
		}

		public float GetDistanceFromSignal(float signal)
		{
			return (1.0f - Mathf.Clamp01(signal)) * this.MaxDistance;
		}

		public float GetSignalFromDistance(float distance)
		{
			return Mathf.Max(0.0f, 1.0f - distance / this.MaxDistance);
		}

		#endregion

		[Serializable]
		public class ConnectionZone
		{
			#region Fields

			[Range(0, 1)]
			public float MinimumSignal;

			#endregion
		}
	}
}