﻿namespace Game.Diagnostics
{
    using Game.Core;

    using UnityEngine;
    using UnityEngine.UI;

    public class DebugGame : MonoBehaviour
    {
        #region Fields

        public Text ConnectedDurationText;

        public GameBehaviour Game;

        public Text RemainingBatteryDurationText;

        public Text ValueText;

        #endregion

        #region Methods

        protected void Update()
        {
            if (this.Game != null)
            {
                if (this.ValueText != null)
                {
                    this.ValueText.text = string.Format("Connection Strength: {0:00}%", this.Game.ConnectionStrength * 100);
                }
                if (this.RemainingBatteryDurationText != null)
                {
                    this.RemainingBatteryDurationText.text = string.Format(
                        "Remaining battery: {0}s",
                        this.Game.RemainingBatteryDuration);
                }
                if (this.ConnectedDurationText != null)
                {
                    this.ConnectedDurationText.text = string.Format(
                        "Connected: {0}s of {1}s",
                        this.Game.ConnectedDuration,
                        this.Game.RequiredConnectedDuration);
                }
            }
        }

        #endregion
    }
}