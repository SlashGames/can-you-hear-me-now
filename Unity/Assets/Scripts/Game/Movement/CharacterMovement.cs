﻿namespace Game.Features.Movement
{
	using Game.Features.Input;

	using UnityEngine;

	public class CharacterMovement : MonoBehaviour
	{
		#region Fields

		public CharacterInput CharacterInput;

		public float KeyboardForce = 100000;

		public float MaxSpeed = 10.0f;

		public Rigidbody2D Rigidbody;

		public float Speed;

		public static CharacterMovement Instance;

		#endregion

		#region Properties

		public Vector2 Direction
		{
			get
			{
				return this.Rigidbody.velocity.normalized;
			}
		}

		#endregion

		#region Methods

		private void Start()
		{
			Instance = this;
		}

		private void FixedUpdate()
		{
			// Get input from the keyboard, with automatic smoothing (GetAxis instead of GetAxisRaw).
			// We always want the movement to be framerate independent, so we multiply by Time.deltaTime.
			var direction = this.CharacterInput.Direction;

			var force = direction * this.KeyboardForce * Time.deltaTime;
			this.Rigidbody.AddRelativeForce(force);

			this.Speed = this.Rigidbody.velocity.magnitude;
		}

		private void Update()
		{
			// Calculate maximum speed.
			this.MaxSpeed = this.KeyboardForce / (this.Rigidbody.mass * this.Rigidbody.drag / Time.fixedDeltaTime);
		}

		#endregion
	}
}