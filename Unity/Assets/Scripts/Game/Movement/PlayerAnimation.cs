﻿using Game.Features.Input;
using UnityEngine;

namespace Game.Movement
{
	public class PlayerAnimation : MonoBehaviour
	{   
		[SerializeField] private CharacterInput _input;
		private float _lookDirection=1;
		private float _targetDirection=1;
		[SerializeField]private float _turnSpeed=0.3f;
		
		public static PlayerAnimation Instance;

		void Awake ()
		{
			Instance = this;
		}
		
		void Update ()
		{
			//update direction
			if (_input.HasInput)
			{
				_targetDirection = _input.Direction.x > 0 ? 1 : -1;
			}

			_lookDirection = Mathf.Lerp(_lookDirection, _targetDirection, Time.deltaTime*_turnSpeed);
			transform.localRotation = Quaternion.Euler(0, _lookDirection * 90 + 90, 0);
		}
	}
}
