﻿namespace Game.Utils
{
    using System;

    using UnityEngine.Events;

    [Serializable]
    public class BooleanUnityEvent : UnityEvent<bool>
    {
    }
}