﻿namespace Game.Utils
{
    /* MinMaxRangeAttribute.cs
    * by Eddie Cameron – For the public domain
    * —————————-
    * Use a MinMaxRange class to replace twin float range values (eg: float minSpeed, maxSpeed; becomes MinMaxRange speed)
    * Apply a [MinMaxRange( minLimit, maxLimit )] attribute to a MinMaxRange instance to control the limits and to show a
    * slider in the inspector
    */
    using System;

    using UnityEngine;

    using Random = UnityEngine.Random;

    public class MinMaxRangeAttribute : PropertyAttribute
    {
        #region Fields

        public float MinLimit;

        public float MaxLimit;

        #endregion

        #region Constructors and Destructors

        public MinMaxRangeAttribute(float minLimit, float maxLimit)
        {
            this.MinLimit = minLimit;
            this.MaxLimit = maxLimit;
        }

        #endregion
    }

    [Serializable]
    public class MinMaxRange
    {
        #region Fields

        public float RangeStart;

        public float RangeEnd;

        #endregion

        #region Public Methods and Operators

        public float GetRandomValue()
        {
            return Random.Range(this.RangeStart, this.RangeEnd);
        }

        #endregion
    }
}