﻿namespace Core.Utils
{
    using Slash.Unity.Common.Loading;

    /// <summary>
    ///   Loads the specified scene immediately after start.
    /// </summary>
    public class LoadSceneOnStartBehaviour : LoadSceneBehaviour
    {
        #region Methods

        private void Start()
        {
            this.Execute();
        }

        #endregion
    }
}