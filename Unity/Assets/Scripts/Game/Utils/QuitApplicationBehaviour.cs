﻿namespace Game.Utils
{
    using UnityEngine;

    public class QuitApplicationBehaviour : MonoBehaviour
    {
        #region Public Methods and Operators

        public void Execute()
        {
            Application.Quit();
        }

        #endregion
    }
}