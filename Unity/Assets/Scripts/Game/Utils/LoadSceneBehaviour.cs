﻿namespace Core.Utils
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    /// <summary>
    ///   Loads the specified scene.
    /// </summary>
    public class LoadSceneBehaviour : MonoBehaviour
    {
        #region Fields

        /// <summary>
        ///   Indicates if the scene should be loaded additive to existing one.
        ///   If loaded additive the old scene objects are not removed.
        /// </summary>
        public bool LoadAdditive;

        /// <summary>
        ///   Name of scene to load.
        /// </summary>
        public string SceneName;

        #endregion

        #region Public Methods and Operators

        [ContextMenu("Execute")]
        public void Execute()
        {
            SceneManager.LoadScene(this.SceneName, this.LoadAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
        }

        #endregion
    }
}