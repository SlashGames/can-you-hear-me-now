﻿namespace LifeApplication.Features.Crafting.Behaviours
{
    using UnityEngine;

    public class SetAnimatorBoolParameter : MonoBehaviour
    {
        #region Fields

        [Tooltip("Name of parameter to set.")]
        public string ParameterName;

        [Tooltip("Target animator to set parameter for.")]
        public Animator Target;

        [Tooltip("Value to set for parameter.")]
        public bool Value;

        #endregion

        #region Public Methods and Operators

        public void Execute()
        {
            this.Execute(this.Value);
        }

        public void Execute(GameObject target)
        {
            var targetAnimator = target != null ? target.GetComponent<Animator>() : null;
            this.Execute(targetAnimator, this.Value);
        }

        public void Execute(bool value)
        {
            this.Execute(this.Target, value);
        }

        #endregion

        #region Methods

        protected void Reset()
        {
            if (this.Target == null)
            {
                this.Target = this.GetComponent<Animator>();
            }
        }

        private void Execute(Animator animator, bool value)
        {
            if (animator != null)
            {
                animator.SetBool(this.ParameterName, value);
            }
        }

        #endregion
    }
}