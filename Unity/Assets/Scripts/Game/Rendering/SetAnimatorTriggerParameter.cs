﻿namespace LifeApplication.Features.Crafting.Behaviours
{
    using UnityEngine;

    public class SetAnimatorTriggerParameter : MonoBehaviour
    {
        #region Fields

        [Tooltip("Name of parameter to set.")]
        public string ParameterName;

        [Tooltip("Target animator to set parameter for.")]
        public Animator Target;

        #endregion

        #region Public Methods and Operators

        public void Execute(GameObject target)
        {
            var targetAnimator = target != null ? target.GetComponent<Animator>() : null;
            this.Execute(targetAnimator);
        }

        public void Execute()
        {
            this.Execute(this.Target);
        }

        #endregion

        #region Methods

        protected void Reset()
        {
            if (this.Target == null)
            {
                this.Target = this.GetComponent<Animator>();
            }
        }

        private void Execute(Animator animator)
        {
            if (animator != null)
            {
                animator.SetTrigger(this.ParameterName);
            }
        }

        #endregion
    }
}