﻿using UnityEngine;
using System.Collections;
using Game.Features.Movement;
using Game.Movement;
using UnityEngine.Networking;

public class CameraMovement : MonoBehaviour
{

	[SerializeField] private float distance=4;
    private Vector3 focusPosition;

    public Transform TargetTransform;

	void Start ()
	{
		GetComponent<Camera>().fieldOfView = 31;
		transform.eulerAngles=new Vector3(309.36f,0,0);
		focusPosition=PlayerAnimation.Instance.transform.position;
		Camera.main.transparencySortMode=TransparencySortMode.Orthographic;
	}
	
    [ContextMenu("Update")]
	void Update ()
	{
		if (Time.time < 1f) focusPosition = GetTargetPoint();

		//float d = Vector3.Distance(PlayerAnimation.Instance.transform.position, focusPosition);
		//if (d > 5)
		//{
		//	focusPosition = Vector3.Lerp(focusPosition, GetTargetPoint(), Time.deltaTime);
		//}

		focusPosition = GetTargetPoint();
		//focusPosition += (Vector3)CharacterMovement.Instance.Rigidbody.velocity.normalized;

		transform.position = focusPosition;

        float currentDistance = distance;
        if (WindManager.Instance != null)
        {
            currentDistance -= WindManager.Instance.Strength * 2;
        }
        transform.Translate(Vector3.back * currentDistance);
	}

	private Vector3 GetTargetPoint()
	{
	    if (TargetTransform != null)
	    {
	        return TargetTransform.position + Vector3.up * 2.5f;
	    }
	    return transform.position;
	}

	void OnDrawGizmos()
	{
		
	}
}
