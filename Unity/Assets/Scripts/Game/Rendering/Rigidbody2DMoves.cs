﻿namespace Game.Rendering
{
    using System;

    using UnityEngine;
    using UnityEngine.Events;

    public class Rigidbody2DMoves : MonoBehaviour
    {
        #region Fields

        public MovementChangedEvent MovementChanged;

        public Rigidbody2D Rigidbody;

        public float VelocityThreshold = 0.01f;

        private bool moves;

        #endregion

        #region Methods

        protected void Reset()
        {
            if (this.Rigidbody == null)
            {
                this.Rigidbody = this.GetComponent<Rigidbody2D>();
            }
        }

        protected void Update()
        {
            if (this.Rigidbody != null)
            {
                var movesNow = this.Rigidbody.velocity.magnitude > this.VelocityThreshold;
                if (movesNow != this.moves)
                {
                    this.moves = movesNow;
                    this.MovementChanged.Invoke(this.moves);
                }
            }
        }

        #endregion

        [Serializable]
        public class MovementChangedEvent : UnityEvent<bool>
        {
        }
    }
}