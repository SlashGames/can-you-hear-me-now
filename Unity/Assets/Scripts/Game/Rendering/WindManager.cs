﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class WindManager : MonoBehaviour
{
	[SerializeField] private Sprite[] _sprites;
	[SerializeField] private float _strength;
	[SerializeField] private float _fadeSpeed = 3;
	private Image _image;
	private int _spriteIndex = 0;
	private float _lastFrameJump = 0;
	[SerializeField] private float _fps;
	public static WindManager Instance;

	public float Strength
	{
		get { return _image.color.a; }
	}

	void Awake ()
	{
		_image = GetComponent<Image>();
		Instance = this;
	    SetWindStrength(0);
	}
	
	void Update ()
	{
		_image.sprite = _sprites[_spriteIndex%_sprites.Length];
		if (Time.time>_lastFrameJump+1.0f/_fps)
		{
			_spriteIndex++;
			_lastFrameJump = Time.time;
        }

        _image.color = Color.Lerp(_image.color, new Color(1, 1, 1, _strength), Time.deltaTime * _fadeSpeed);
	}

	public void SetWindStrength(float strength)
	{
	    _strength = strength + 0.3f;
	}

	public void SetDirection(Vector2 direction)
	{
		if ((direction.x > 0) ^ (direction.y > 0))
		{
			_image.rectTransform.localScale = new Vector2(-1, 1);
		}
		else
		{
			_image.rectTransform.localScale = new Vector2(1, 1);
		}
	}
}
