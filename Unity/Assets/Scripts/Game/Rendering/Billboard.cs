﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Billboard.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FlyingFortress.Unity.Features.Camera.Behaviours
{
    using UnityEngine;

    public class Billboard : MonoBehaviour
    {
        #region Fields

        public Transform RotationAnchor;

        public float SpriteRotationOffset;

        public Camera BillboardCamera;

        private Quaternion cameraRotation;

        #endregion

        #region Methods

        protected void Start()
        {
            this.BillboardCamera = Camera.main;

            // Get visual rotation.
            if (this.BillboardCamera != null)
            {
                this.cameraRotation = this.BillboardCamera.transform.rotation;
            }

            this.UpdateRotation();
        }

        protected void Reset()
        {
            if (this.RotationAnchor == null)
            {
                this.RotationAnchor = this.transform.parent;
            }

            if (this.BillboardCamera == null)
            {
                this.BillboardCamera = Camera.main;
            }

            // Get visual rotation.
            if (this.BillboardCamera != null)
            {
                this.cameraRotation = this.BillboardCamera.transform.rotation;
            }
        }

        [ContextMenu("Update")]
        protected void Update()
        {
            this.UpdateRotation();
        }

        private void UpdateRotation()
        {
            // Copy 2D rotation from parent.
            var rotation = -this.RotationAnchor.rotation.eulerAngles.y;
            const float IsometricRotation = 45;
            this.transform.rotation = this.cameraRotation
                                           * Quaternion.Euler(
                                               0,
                                               0,
                                               rotation + IsometricRotation - this.SpriteRotationOffset);
        }

        #endregion
    }
}