﻿using Network;

namespace Game.UI.IncomingCall
{
	using System;

	using UnityEngine;
	using UnityEngine.Events;

	public class IncomingCallBehaviour : MonoBehaviour
	{
		#region Fields

		public AnswerEvent Answer;

		public HangUpEvent HangUp;

		#endregion

		#region Public Methods and Operators

		private void Start()
		{

		}

		public void OnAnswer()
		{
			this.Answer.Invoke();

		    if (DataTransfer.Local != null)
		    {
		        DataTransfer.Local.RpcAcceptCall();
		    }
		}

		public void OnHangUp()
		{
			this.HangUp.Invoke();
		    
            if (DataTransfer.Local != null)
		    {
		        DataTransfer.Local.RpcRefuseCall();
		    }
		}

		#endregion

		[Serializable]
		public class HangUpEvent : UnityEvent
		{
		}

		[Serializable]
		public class AnswerEvent : UnityEvent
		{
		}
	}
}