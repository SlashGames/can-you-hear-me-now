﻿namespace Game.Audio
{
    using UnityEngine;

    public class PlaySound : MonoBehaviour
    {
        #region Fields

        public AudioClip Clip;

        public AudioSource Source;

        #endregion

        #region Methods
        
        public void Play()
        {
            if (this.Source != null && this.Clip != null)
            {
                this.Source.clip = this.Clip;
                this.Source.loop = true;
                this.Source.Play();
            }
        }

        public void Stop()
        {
            if (this.Source != null)
            {
                this.Source.Stop();
            }
        }

        #endregion
    }
}