﻿namespace KattaGod.Core
{
    using UnityEngine;

    public class TriggerOneShotSound : MonoBehaviour
    {
        #region Fields

        public AudioClip Clip;

        public AudioSource Source;

        #endregion

        #region Public Methods and Operators

        public void Trigger()
        {
            if (this.Source != null && this.Clip != null)
            {
                this.Source.PlayOneShot(this.Clip);
            }
        }

        #endregion
    }
}