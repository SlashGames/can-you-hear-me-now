﻿namespace Game.Audio
{
    using UnityEngine;

    public class SelectRandomClip : MonoBehaviour
    {
        #region Fields

        public AudioClip[] Clips;

        public AudioSource Source;

        #endregion

        #region Public Methods and Operators

        public void Start()
        {
            if (this.Source != null && this.Clips.Length > 0)
            {
                this.Source.clip = this.Clips[Random.Range(0, this.Clips.Length)];
                this.Source.Play();
            }
        }

        #endregion
    }
}