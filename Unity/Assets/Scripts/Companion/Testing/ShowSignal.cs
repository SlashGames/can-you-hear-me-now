﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Companion.Testing
{
	public class ShowSignal : MonoBehaviour
	{
		[SerializeField] private Text _uiText;

		private void Update()
		{

			if (Network.DataTransfer.DesktopInstance)
			{
				_uiText.text = Network.DataTransfer.DesktopInstance.Signal.ToString();
			}
		}
	}
}
