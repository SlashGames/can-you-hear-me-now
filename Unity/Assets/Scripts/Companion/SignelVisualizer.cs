﻿using UnityEngine;
using System.Collections;
using Network;
using UnityStandardAssets.ImageEffects;

public class SignelVisualizer : MonoBehaviour
{

	[SerializeField] private NoiseAndScratches _noiseAndScratches;
	
	void Update ()
	{
		_noiseAndScratches.grainIntensityMax = (1-DataTransfer.DesktopInstance.Signal)*5;
		_noiseAndScratches.enabled = DataTransfer.DesktopInstance.Signal < 0.92f;
	}
}
