﻿using System.Collections;
using UnityEngine;
using System.Linq;
using Network;
using UnityEngine.UI;

public class CompanionManager : MonoBehaviour
{

	[SerializeField] private GameObject _startScreen;
	[SerializeField] private GameObject _connectingScreen;
	[SerializeField] private GameObject _callScreen;
	[SerializeField] private GameObject _looseScreen;
	[SerializeField] private Button _restartButton;

	[SerializeField] private GameObject _successImage;
	private GameObject[] _allScreens;


	public void Start()
	{
		DataTransfer.OnCallRefused += this.DoHangup;
		DataTransfer.OnCallAccepted += this.OnCallAccepted;
		DataTransfer.OnBatteryEmpty += this.OnBatteryLost;
		DataTransfer.OnWin += () =>
		{
			StartCoroutine(this.DoWin());
		};
		_allScreens = new[] {_startScreen, _connectingScreen, _callScreen, _looseScreen};

		ShowStartScreen();
	}

	public void ShowStartScreen()
	{
		SetActiveScreen(_startScreen);
	}

	private void OnCallAccepted()
	{
		SetActiveScreen(_callScreen);
	}

	public void PressCallButton()
	{
		SetActiveScreen(_connectingScreen);
		if (DataTransfer.MobileInstance)
		{
			DataTransfer.MobileInstance.CmdStartCall();
		}
	}

	public void DoHangup()
	{
		SetActiveScreen(_startScreen);
		//todo play hangup sound
	}

	private void OnBatteryLost()
	{
		SetActiveScreen(_looseScreen);
		_restartButton.interactable = false;
		StartCoroutine(EnableRestartButton());
	}

	IEnumerator EnableRestartButton()
	{
		yield return new WaitForSeconds(5);
		_restartButton.interactable = true;
	}

	IEnumerator DoWin()
	{
		_successImage.SetActive(true);
		yield return new WaitForSeconds(5);
		_successImage.SetActive(true);
		ShowStartScreen();
	}

	private void SetActiveScreen(GameObject newScreen)
	{
		foreach (var screen in _allScreens)
		{
			screen.SetActive(screen==newScreen);
		}
	}
}
