﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ActivateButtonOnNetworkConnected : MonoBehaviour
{

	private Button selectable;

	private void Awake()
	{
		selectable = GetComponent<Button>();
	}

	void Update ()
	{
#if !UNITY_EDITOR
		selectable.interactable = NetworkManager.singleton.IsClientConnected();
#endif
	}
}
