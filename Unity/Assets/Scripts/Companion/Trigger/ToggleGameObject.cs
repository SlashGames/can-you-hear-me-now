﻿using UnityEngine;
using System.Collections;

public class ToggleGameObject : MonoBehaviour
{
	[SerializeField] private GameObject target;
	public void DoToggle()
	{
		target.SetActive(!target.activeSelf);
	}
}
