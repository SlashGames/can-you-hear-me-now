﻿using UnityEngine;
using System.Collections;
using System;
using Game.Logic;
using Network;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class AudioManager : MonoBehaviour
{

	[SerializeField] private AudioSource[] _worst;
	[SerializeField] private AudioSource[] _bad;
	[SerializeField] private AudioSource[] _medium;
	[SerializeField] private AudioSource[] _good;

	[SerializeField] private AudioSource _staticNoise;

	[SerializeField]private AnimationCurve _noiseVolumeCurve;
	[SerializeField]private AnimationCurve _noisePitchCurve;

	[SerializeField] private NoiseAndScratches _noiseEffect;

	//[SerializeField]private AnimationCurve _noiseVolumeCurve;
	//[SerializeField]private AnimationCurve _noisePitchCurve;

	private float _timeToNextSound=6;
	private int _lastSoundIndex = -1;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{

		float signal = 0;
		if (DataTransfer.DesktopInstance) {
			signal = DataTransfer.DesktopInstance.Signal;
		}

		_timeToNextSound -= Time.deltaTime;

		if (_timeToNextSound < 0)
		{
			AudioSource[] currentSources;
			if (signal > ConnectionSettings.Instance.Zones[0].MinimumSignal)
			{
				currentSources = _good;

			}
			else
			{
				if (signal > ConnectionSettings.Instance.Zones[1].MinimumSignal)
				{
					currentSources = _medium;
				}
				else
				{
					if (signal > ConnectionSettings.Instance.Zones[2].MinimumSignal)
					{
						currentSources = _bad;
					}
					else {
						currentSources = _worst;
					}
				}
			}

			int randomIndex = -1;
			do
			{
				randomIndex = (UnityEngine.Random.Range(0, currentSources.Length - 1));
			} while (randomIndex == _lastSoundIndex);
			_lastSoundIndex = randomIndex;

			AudioSource source = currentSources[randomIndex];
			_timeToNextSound += source.clip.length + 1 + UnityEngine.Random.value*2;
			source.Play();
		}

		print(signal);
		_staticNoise.volume = _noiseVolumeCurve.Evaluate(signal);
		_noiseEffect.grainIntensityMin = _noiseVolumeCurve.Evaluate(signal) * 5;
		_staticNoise.pitch = _noisePitchCurve.Evaluate(signal);
	}
}
