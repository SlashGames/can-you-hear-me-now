﻿using Network;
using UnityEngine;

namespace Companion.Interface
{
	public class FigureMovement : MonoBehaviour
	{
		private Vector3 targetPosition;
		private Vector3 _velocity;
		private Vector3 _currentPosition;


		void Update ()
		{
			_currentPosition = Vector3.SmoothDamp(_currentPosition, targetPosition, ref _velocity, 1);
			if (Random.value < Time.deltaTime)
			{
				targetPosition = Random.insideUnitCircle*1.5f;
			}

			if (DataTransfer.DesktopInstance.Signal < 0.95f)
			{
				if (Random.value < Time.deltaTime*7)
				{
					transform.position = targetPosition;
				}
			}
		}
	}
}
