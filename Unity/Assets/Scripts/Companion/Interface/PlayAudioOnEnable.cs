﻿using UnityEngine;
using System.Collections;

public class PlayAudioOnEnable : MonoBehaviour {


	void OnEnable()
	{
		var source = GetComponent<AudioSource>();
		if (source != null)
		{
			source.Play();
		}
	}
}
