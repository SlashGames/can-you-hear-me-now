﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class DrawWebCamTexture : MonoBehaviour
{
	private WebCamTexture _camTexture;

	void Start () {
		_camTexture=new WebCamTexture(WebCamTexture.devices.Last().name);
		_camTexture.Play();
		GetComponent<RawImage>().texture = _camTexture;
	}
	
	void Update () {
	
	}
}
