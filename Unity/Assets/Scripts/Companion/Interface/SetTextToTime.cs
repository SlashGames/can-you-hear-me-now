﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class SetTextToTime : MonoBehaviour
{

	[SerializeField] private Text _text;

	void Update () {
		_text.text=System.DateTime.Now.Hour+":"+System.DateTime.Now.Minute;
	}
}
