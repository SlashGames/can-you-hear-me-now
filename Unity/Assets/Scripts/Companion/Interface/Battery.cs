﻿using UnityEngine;
using System.Collections;
using Network;
using UnityEngine.UI;

public class Battery : MonoBehaviour
{
	private Image _battery;
	[SerializeField] private Gradient _colorGradient=new Gradient();

	void Start ()
	{
		_battery = GetComponent<Image>();
	}
	

	void Update ()
	{
		_battery.fillAmount = DataTransfer.DesktopInstance.BatteryRatio;
		_battery.color = _colorGradient.Evaluate(DataTransfer.DesktopInstance.BatteryRatio);
	}
}
