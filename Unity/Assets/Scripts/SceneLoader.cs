﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
	[SerializeField] private string _mobileScene;
	[SerializeField] private string _desktopScene;

	[SerializeField] private bool _loadMobile;

	private void Start()
	{
		Debug.Log("Start SceneLoader");
#if UNITY_EDITOR
		if(_loadMobile)
#else
		if (Application.isMobilePlatform)
#endif
		{
			SceneManager.LoadScene(_mobileScene, LoadSceneMode.Additive);
		}
		else
		{
			SceneManager.LoadScene(_desktopScene , LoadSceneMode.Additive);
		}
	}
}
