﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace Network
{
	public class DataTransfer : NetworkBehaviour
	{
		public static DataTransfer DesktopInstance;
		public static DataTransfer MobileInstance;

		[SyncVar,SerializeField] private float _signal;

		public float Signal { get { return _signal; } }

		public static DataTransfer Local
		{
			get
			{
				if (Application.isMobilePlatform)
				{
					return MobileInstance;
				}
				else
				{
					return DesktopInstance;
				}
			}
		}

		[SyncVar]
		public float BatteryRatio;

		public static Action OnIncommingCall;
		public static Action OnCallRefused;
		public static Action OnCallAccepted;
		public static Action OnBatteryEmpty;
		public static Action OnWin;

		public void Start()
		{
			if (Application.isMobilePlatform ^ isLocalPlayer)
			{
				DesktopInstance = this;
			}
			else
			{
				MobileInstance = this;
			}
		}

		public void UpdateConnectionValue(float value)
		{
			if(!isServer)return;
			_signal = value;
		}

		[Command]
		public void CmdStartCall()
		{
			if (OnIncommingCall != null) {
				OnIncommingCall();
			}
		}

		[ClientRpc]
		public void RpcRefuseCall()
		{
			if (OnCallRefused != null) {
				OnCallRefused();
			}
		}
		[ClientRpc]
		public void RpcAcceptCall()
		{
			if (OnCallAccepted != null) {
				OnCallAccepted();
			}
		}

		[ClientRpc]
		public void RpcBatteryEmpty()
		{
			if (OnBatteryEmpty != null) {
				OnBatteryEmpty();
			}
		}
		[ClientRpc]
		public void RpcWin()
		{
			if (OnWin != null) {
				OnWin();
			}
		}
	}
}
