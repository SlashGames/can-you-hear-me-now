﻿using UnityEngine;
using UnityEngine.Networking;

namespace Network
{
	public class Connector : NetworkDiscovery
	{
		public void Start()
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			useGUILayout = false;
			if (Application.isMobilePlatform) {
				isClient = true;
				isServer = false;
				Initialize();
			} else {
				NetworkManager.singleton.maxConnections = 10;
				NetworkManager.singleton.StartHost();
				isServer = true;
				isClient = false;
				Initialize();
			}
		}


		public override void OnReceivedBroadcast(string fromAddress, string data)
		{
			if (NetworkManager.singleton.IsClientConnected())
				return;
			NetworkManager.singleton.networkAddress = fromAddress;
			NetworkManager.singleton.StartClient();
			//ClientScene.AddPlayer(0);
		}
	}
}