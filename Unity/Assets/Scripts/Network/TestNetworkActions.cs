﻿using UnityEngine;
using System.Collections;
using Network;

public class TestNetworkActions : MonoBehaviour {
	
	void Start ()
	{
		DataTransfer.OnIncommingCall += () => {
			print("OnIncommingCall");
		};

		DataTransfer.OnCallRefused += () => {
			print("OnCallRefused");
		};

		DataTransfer.OnCallAccepted += () => {
			print("OnCallAccepted");
		};
	}
}
